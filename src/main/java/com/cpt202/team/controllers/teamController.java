package com.cpt202.team.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.ArrayList;
import com.cpt202.team.models.Team;
import com.cpt202.team.services.TeamService;



//Spring Annotation
@RestController
@RequestMapping("/team")
public class TeamController {
    @Autowired
    private TeamService teamService;
    
    @GetMapping("/list")
    public List<Team> getList(){
        return teamService.getTeamList();
    }

      
    @PostMapping("/add")
    public Team addTeam(@RequestBody Team team){
        return teamService.newTeam(team);

    }

}
